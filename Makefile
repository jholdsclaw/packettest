.PHONY: clean All

All:
	@echo "----------Building project:[ packettest - Debug ]----------"
	@$(MAKE) -f  "packettest.mk"
clean:
	@echo "----------Cleaning project:[ packettest - Debug ]----------"
	@$(MAKE) -f  "packettest.mk" clean
